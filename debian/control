Source: linphone
Section: sound
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Kilian Krause <kilian@debian.org>,
           Tzafrir Cohen <tzafrir@debian.org>
Build-Depends: autoconf,
               automake,
               autotools-dev,
               debhelper (>= 9),
               dh-autoreconf,
               doxygen,
               intltool,
               libasound2-dev [linux-any],
               libatk1.0-dev,
               libavcodec-dev (>= 6:10~),
               libavutil-dev (>= 6:10~),
               libcairo2-dev,
               libcunit1-dev,
               libexosip2-dev (>= 4),
               libfontconfig1-dev,
               libfreetype6-dev,
               libgdk-pixbuf2.0-dev,
               libglade2-dev,
               libglew-dev,
               libglib2.0-dev,
               libgtk2.0-dev,
               libncurses5-dev,
               libnotify-dev,
               libogg-dev,
               libopus-dev,
               libosip2-dev (>= 4),
               libpango1.0-dev,
               libpcap-dev,
               libpulse-dev,
               libreadline-dev,
               libreadline6-dev,
               libsdl1.2-dev,
               libsoup2.4-dev,
               libspandsp-dev,
               libspeex-dev,
               libspeexdsp-dev,
               libsqlite3-dev,
               libswscale-dev (>= 6:10~),
               libtheora-dev,
               libtiff-dev,
               libtool,
               libudev-dev [linux-any],
               libupnp-dev,
               libv4l-dev [linux-any],
               libvpx-dev,
               libxext-dev,
               libxml-parser-perl,
               libxv-dev,
               mesa-common-dev,
               pkg-config,
               sgmltools-lite,
               vim-common
# Requires updated libsrtp-dev
# libsrtp-dev [!hurd-any !sparc], libzrtpcpp-dev (>= 2.1),
#
# OpenSSL requires upstream licence update
# libssl-dev,
Standards-Version: 3.9.6
Homepage: http://www.linphone.org/
Vcs-Git: https://anonscm.debian.org/git/pkg-voip/linphone.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-voip/asterisk.git

Package: linphone
Architecture: any
Depends: linphone-nogtk (=${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: yelp
Description: SIP softphone - graphical client
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 The main features of linphone are:
   - a nice graphical interface;
   - it includes a large variety of codecs with different quality / bandwidths;
   - it uses the well-known and standardised SIP protocol.

Package: linphone-dbg
Section: debug
Priority: extra
Depends: linphone (= ${binary:Version}), ${misc:Depends}
Architecture: any
Description: Debugging symbols for linphone
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 This package contains the debugging symbols.

Package: linphone-nogtk
Depends: host,
         liblinphone5 (= ${binary:Version}),
         linphone-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Architecture: any
Description: SIP softphone - console-only client
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 This package contains the console version of linphone.

Package: linphone-common
Depends: ${misc:Depends}
Architecture: all
Description: Shared components of the linphone SIP softphone
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 This package contains the resource files of linphone (the rings).

Package: liblinphone5
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Architecture: any
Multi-Arch: same
Description: Linphone's shared library part (supporting the SIP protocol)
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 This package contains the shared runtime libraries.

Package: liblinphone-dev
Section: libdevel
Depends: liblinphone5 (= ${binary:Version}),
         libmediastreamer-dev,
         libortp-dev,
         ${misc:Depends}
Architecture: any
Description: Linphone web phone's library - development files
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 This package contains the files needed to use the linphone's library in your
 programs.

Package: libmediastreamer-base3
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Architecture: any
Multi-Arch: same
Description: Linphone web phone's media library
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 This package contains the shared runtime libraries for handling media
 operations.

Package: libmediastreamer-dev
Section: libdevel
Depends: libmediastreamer-base3 (= ${binary:Version}),
         libortp-dev,
         ${misc:Depends}
Architecture: any
Description: Linphone web phone's media library - development files
 Linphone is an audio and video internet phone using the SIP protocol. It
 has a GTK+ and console interface, includes a large variety of audio and video
 codecs, and provides IM features.
 .
 This package contains the development libraries for handling media operations.

Package: libortp9
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Architecture: any
Multi-Arch: same
Description: Real-time Transport Protocol stack
 This library implements the RFC 1889 (RTP) with a easy to use API with high
 and low level access.

Package: libortp-dev
Section: libdevel
Depends: libortp9 (= ${binary:Version}), ${misc:Depends}
Architecture: any
Description: Real-time Transport Protocol stack - development files
 This library implements the RFC 1889 (RTP) with a easy to use API with high
 and low level access.
 .
 The main features are:
   - support for multiple profiles, AV profile (RFC 1890) being the default one;
   - an optional packet scheduler for synchronizing RTP recv and send;
   - implements blocking and non blocking IO for RTP sessions;
   - supports multiplexing IO;
   - supports part of RFC 2833 for telephone events over RTP.
 .
 This package contains the development files.
