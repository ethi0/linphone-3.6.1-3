dnl -*- autoconf -*-
AC_DEFUN([LP_SETUP_EXOSIP],[
AC_REQUIRE([AC_CANONICAL_HOST])
AC_REQUIRE([LP_CHECK_OSIP2])


case $host_alias in
	i386-apple*|armv6-apple*|armv7-apple*|armv7s-apple*)
		OSIP_LIBS="$OSIP_LIBS  -framework CoreFoundation -framework CFNetwork -lresolv"
	;;
	x86_64-apple*)
		OSIP_LIBS="$OSIP_LIBS  -framework CoreFoundation"
	;;
esac


dnl eXosip embeded stuff
EXOSIP_CFLAGS="$OSIP_CFLAGS -DOSIP_MT "
EXOSIP_LIBS="$OSIP_LIBS -leXosip2 "


dnl check for eXosip2 headers
AC_CHECK_HEADERS([eXosip2/eXosip.h],
        [], [AC_MSG_ERROR([Could not find eXosip2 headers])]
)

dnl check for eXosip2 libs
AC_SEARCH_LIBS([eXosip_init], [eXosip2],
        [], [AC_MSG_ERROR([Could not find eXosip2 library])]
)

AC_CHECK_FUNCS([ eXosip_malloc eXosip_set_tls_ctx eXosip_get_version eXosip_tls_verify_certificate eXosip_tls_verify_cn eXosip_trylock eXosip_reset_transports ])

if test "x$ac_cv_func_eXosip_malloc" == xyes; then
        AC_DEFINE([HAVE_STRUCT_EXOSIP_T], [1], [Define if struct eXosip_t exists.])
elif test "x$ac_cv_func_eXosip_set_tls_ctx" != xyes; then
        AC_MSG_ERROR([Could not find eXosip2 library with version >= 3.5.0 !])
fi


AC_SUBST(EXOSIP_CFLAGS)
AC_SUBST(EXOSIP_LIBS)
])
